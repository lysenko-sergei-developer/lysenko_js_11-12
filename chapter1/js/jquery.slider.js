(function($) {
		var leftUIEL = $('.carousel-arrow-left');
		var rightUIEL = $('.carousel-arrow-right');
		var elementsList = $('.carousel-list');

		var pixelsOffset = 125;
		var currentLeftValue = 0;
		var elementsCount = elementsList.find('li').length;
		var minimumOffset = -((elementsCount) * pixelsOffset);
		var maximumOffset = 0;

		$.fn.carouselPlugin = function() {
			leftUIEL.click(function() {
				if (currentLeftValue != maximumOffset) {
					currentLeftValue += 125;
					elementsList.animate({
						left: currentLeftValue + "px"
					}, 500);
				}
			});

			rightUIEL.click(function() {
				if (currentLeftValue != minimumOffset) {
					currentLeftValue -= 125;
					elementsList.animate({
						left: currentLeftValue + "px"
					}, 500);
				}
			});

	    return this;
    }
})(jQuery);
