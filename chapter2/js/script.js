$(function() {

  var data = {
    about: {
      name: "ЛЫСЕНКО СЕРГЕЙ СЕРГЕЕВИЧ",
      photo: "http://i.imgur.com/oguY0pP.png",
      description: "Временно выращиваю сверхбыстрых хомячков в CERN",
    },
    mission: {
      message: "Хочу учить фронтенд, потому что:",
      reasons: [
        "Хомячки меня разочаровали",
        "Не могу поддержать срач Angular vs ReactJs",
        "$$$",
        "Хочу работать 25 часов в сутки",
        "В программировании тренды постоянно меняются и ты не заскучаешь",
        "Люблю креативить",
        "После того как я рассекретил себя, меня уволят с CERN :( и нужно искать новую работу",
      ],
    },
    phoneContacts: {
      description: "Мой контактный телефон:",
      phoneNumber: "+380664754968",
    },
    vkontakteContacts: {
      description: "Мой профиль Вконтакте:",
      url: {
        link: "https://vk.com/p1ratxd",
        description: "my vk account",
      },
    },
    feedback: {
      description: "Мой фидбек:",
      joke: "В наличии есть пару сверхзвуковых и один сверхсветовой хомячок",
    },
  };

  var $biographyTemplate = $('#biography-template').html();
  var compiled = _.template($biographyTemplate);
  var content = compiled(data);

  $('body').append(content);
});
